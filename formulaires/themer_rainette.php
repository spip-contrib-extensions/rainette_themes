<?php
/**
 * Gestion du formulaire de visualisation des thèmes du plugin Rainette.
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * On surcharge la fonction de chargement par défaut afin de fournir le service, sa configuration
 * et son état d'exécution au formulaire.
 *
 * @return array Tableau des données à charger par le formulaire.
 */
function formulaires_themer_rainette_charger() {
	// Initialiser l'indicateur d'édition
	$valeurs = [
		'editable' => false,
	];

	if (autoriser('configurer')) {
		// Initialisation des paramètres du formulaire.
		$valeurs['operation'] = _request('operation');
		$valeurs['service'] = _request('service');
		$valeurs['theme'] = _request('theme');
		$valeurs['theme_2'] = _request('theme_2');
		$valeurs['icone'] = _request('icone');
		$valeurs['periode'] = _request('periode');

		// Initialiser la liste de toutes les opérations
		include_spip('inc/rainette_themer');
		$valeurs['_operations']['afficher_theme'] = _T('rainette:label_operation_afficher_theme');
		$valeurs['_operations']['comparer_theme'] = _T('rainette:label_operation_comparer_theme');
		$valeurs['_operations']['comparer_icone'] = _T('rainette:label_operation_comparer_icone');
		if (_RAINETTE_DEBUG_TRANSCODAGE) {
			$valeurs['_operations']['transcoder_weather'] = _T('rainette:label_operation_transcoder_weather');
		}

		// Liste des services :
		// -- On ne sélectionne les services actifs et inactifs car ils peuvent servir de référence et
		//    on rajoute weather car il ne fait plus partie des services utilisés.
		include_spip('inc/rainette_normaliser');
		include_spip('rainette_fonctions');
		$services = rainette_lister_services('tableau', false);
		// -- Ajouter weather pour pouvoir afficher ses thèmes
		$services['weather'] = [
			'nom' => 'weather.com&reg;', 'actif' => false
		];
		// -- Construire le tableau de la saisie des services et les opérations non autorisées par service
		foreach ($services as $_service => $_description) {
			// Activité du service
			$activite = $_description['actif'] ? _T('rainette:service_actif') : _T('rainette:service_inactif');
			$valeurs['_services'][$_service] = $_description['nom'] . " ({$activite})";

			// Configuration du service
			$configuration_service = configuration_service_lire($_service, 'service');

			// Opérations non autorisées
			$valeurs['_operations_non_autorisees'][$_service] = [];
			// -- pas de transcodage vers weather
			if (!in_array('weather', $configuration_service['icones']['origines'])) {
				$valeurs['_operations_non_autorisees'][$_service][] = 'transcoder_weather';
			}
			// Suivant le nombre de thèmes locaux d'un service, certaines opérations sont possibles ou pas
			$nb_themes = count(rainette_lister_themes($_service, 'local'));
			if ($nb_themes < 2) {
				$valeurs['_operations_non_autorisees'][$_service][] = 'comparer_theme';
				$valeurs['_operations_non_autorisees'][$_service][] = 'comparer_icone';
			}
			if ($nb_themes < 1) {
				$valeurs['_operations_non_autorisees'][$_service][] = 'afficher_theme';
			}
		}
		$valeurs['_service_defaut'] = rainette_service_defaut();

		// -- Si on a déjà choisi un service, on ajoute la liste des thèmes ou des codes d'icones suivant
		//    le type d'opération choisi (étape 2).
		if (
			$valeurs['operation'] and $valeurs['service']
		) {
			if (($valeurs['operation'] !== 'comparer_icone')) {
				$valeurs['_themes'] = rainette_lister_themes($valeurs['service'], 'local');
				if (($valeurs['operation'] === 'comparer_theme')) {
					// -- si on compare deux thèmes du même service on évite de se positionner sur le même thème
					$theme_ids = array_keys($valeurs['_themes']);
					$valeurs['_defaut_themes_2'] = $theme_ids[1];
				} elseif (($valeurs['operation'] === 'transcoder_weather')) {
					$valeurs['_themes_weather'] = rainette_lister_themes('weather', 'local');
				}
			} else {
				$valeurs['_icones'] = rainette_lister_codes($valeurs['service']);
			}
		}

		// Préciser le nombre d'étapes du formulaire
		$valeurs['_etapes'] = 2;

		// Edition possible
		$valeurs['editable'] = true;
	}

	return $valeurs;
}

/**
 * @return array
 */
function formulaires_themer_rainette_traiter() {
	$retour = [];

	$operation = _request('operation');
	$service = _request('service');
	$periode = _request('periode');
	if ($periode === null) {
		$periode = 0;
	}

	// Acquérir la configuration du service concerné
	include_spip('inc/rainette_normaliser');
	$configuration = configuration_service_lire($service, 'service');

	// Initialisation du contexte commun de choix des icones
	$parametres_icone = [
		'service'         => $service,
		'transcodages'    => $configuration['icones']['transcodages'],
		'theme_origine'   => 'local',
		'periode_stricte' => true,
	];

	// Traiter l'opération choisie
	include_spip('inc/rainette_themer');
	if ($operation === 'comparer_icone') {
		// Pour cette opération, on affiche l'icone représentant le même code pour tous les thèmes du service
		// -- Récupérer la liste des thèmes.
		include_spip('rainette_fonctions');
		$themes = rainette_lister_themes($service, 'local');

		// -- Construire la liste des icones
		include_spip('inc/rainette_convertir');
		$code = _request('icone');
		$icone['code'] = $code;
		$icone['code_meteo'] = $code;
		$icones = [];
		foreach ($themes as $_theme => $_titre) {
			$parametres_icone['theme_id'] = $_theme;
			$icone['source'] = code_meteo2icone($code, $periode, $parametres_icone);
			$icones[$_theme] = [
				'resume' => $_titre,
				'icone'  => $icone
			];
		}
		$retour['message_ok']['icones'] = $icones;

		// -- Détermination du squelette à utiliser pour l'affichage.
		$retour['message_ok']['inclusion'] = 'comparer_icone';
	} else {
		// Pour ces opérations, on a toujours besoin de récupérer le jeu d'icones
		// du premier thème choisi et l'affichage est toujours celui d'un thème complet
		// -- Récupération de la liste des icônes.
		$parametres_icone['theme_id'] = _request('theme');
		$icones = rainette_lister_icones($periode, $parametres_icone);
		$retour['message_ok']['icones'][] = $icones;
		// -- Détermination du squelette à utiliser pour l'affichage.
		$retour['message_ok']['inclusion'] = 'afficher_theme';

		// Si on veut comparer deux thèmes complets, on acquiert le jeu du deuxième thème choisi.
		// On traite le cas où le deuxième thème est un de weather (transcodage_weather).
		if (
			($operation === 'comparer_theme')
			or ($operation === 'transcoder_weather')
		) {
			// -- Récupération de la liste des icônes du deuxième thème
			$parametres_icone['theme_origine'] = $operation === 'comparer_theme' ? 'local' : 'weather';
			$parametres_icone['theme_id'] = _request('theme_2');
			$icones = rainette_lister_icones($periode, $parametres_icone);
			$retour['message_ok']['icones'][] = $icones;
		}
	}

	$retour['editable'] = true;

	return $retour;
}

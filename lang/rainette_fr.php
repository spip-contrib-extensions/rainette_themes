<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/rainette_themes.git

return [

	// B
	'bouton_continuer' => 'Passer à l’étape 2',
	'bouton_retourner' => 'Retourner à l’étape 1',

	// E
	'explication_themer_icone' => 'Choisissez un code d’icone à comparer entre tous les thèmes du service et valider pour les afficher en dessous du formulaire.',
	'explication_themer_operation' => 'La liste des opérations dépend du service.',
	'explication_themer_theme' => 'Choisissez un thème et valider pour l’afficher en dessous du formulaire.',
	'explication_themer_theme_2' => 'Choisissez un thème à comparer avec le thème de base et valider pour les afficher en regard en dessous du formulaire.',
	'explication_transcoder_theme' => 'Choisissez un thème de base du service pour illustrer le transcodage.',
	'explication_transcoder_weather' => 'Choisissez un thème de weather.com® à comparer avec le thème de base et valider pour afficher le transcodage en dessous du formulaire.',

	// I
	'info_etape' => 'Etape @etape@ / @etapes@',
	'info_icone_commun' => 'Icones communs jour et nuit',
	'info_page_themer' => 'Rainette autorise l’utilisation de thèmes d’icones adaptés à chaque service. Le plugin « @plugin@ » installe de nombreux thèmes supplémentaires et permet de les visualiser et de les comparer.',

	// L
	'label_icone_comparaison' => 'Icone à comparer',
	'label_icone_jour' => 'Jour',
	'label_icone_nuit' => 'Nuit',
	'label_operation' => 'Type de consultation',
	'label_operation_afficher_theme' => 'Afficher le thème complet d’un service',
	'label_operation_comparer_icone' => 'Comparer le même icone de tous les thèmes d’un service',
	'label_operation_comparer_theme' => 'Comparer deux thèmes d’un même service',
	'label_operation_transcoder_weather' => 'Vérifier le transcodage d’un service vers weather.com®',
	'label_periode' => 'Version des icones',
	'label_service' => 'Service météorologique',
	'label_theme_1' => 'Thème d’icones de base',
	'label_theme_2' => 'Thème d’icones à comparer',
	'label_theme_weather' => 'Thème d’icones weather.com®',

	// T
	'titre_menu_themes' => 'Thèmes',
	'titre_menu_themes_consultation' => 'Consultation',
	'titre_page_themer' => 'Consulter les thèmes',
];

<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/rainette-rainette_themes?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// B
	'bouton_continuer' => 'Go to step 2',
	'bouton_retourner' => 'Go back to step 1',

	// E
	'explication_themer_icone' => 'Select an icon code to compare across all the service’s themes and validate to display them below the form.',
	'explication_themer_operation' => 'The list of operations is service-dependent.',
	'explication_themer_theme' => 'Select a theme and validate to display it below the form.',
	'explication_themer_theme_2' => 'Select a theme to compare with the default theme and validate to display them both below the form.',
	'explication_transcoder_theme' => 'Select a service theme to illustrate transcoding.',
	'explication_transcoder_weather' => 'Select a weather.com® theme to compare with the default theme of the selected service and validate to display the transcoding below the form.',

	// I
	'info_etape' => 'Step @etape@ / @etapes@',
	'info_icone_commun' => 'Common icons for day and night',
	'info_page_themer' => 'Rainette allows the use of icon themes adapted to each service. The "@plugin@" plugin installs many additional themes, allowing you to explore and compare them.',

	// L
	'label_icone_comparaison' => 'Icon to compare',
	'label_icone_jour' => 'Day',
	'label_icone_nuit' => 'Night',
	'label_operation' => 'Type of consultation',
	'label_operation_afficher_theme' => 'Display the complete theme of a service',
	'label_operation_comparer_icone' => 'Compare the same icon across all themes in a service',
	'label_operation_comparer_theme' => 'Compare two themes from the same service',
	'label_operation_transcoder_weather' => 'Check the transcoding of a service to weather.com®.',
	'label_periode' => 'Icon version',
	'label_service' => 'Weather service',
	'label_theme_1' => 'Basic icon theme',
	'label_theme_2' => 'Icon theme to compare',
	'label_theme_weather' => 'weather.com® icon theme',

	// T
	'titre_menu_themes' => 'Themes',
	'titre_menu_themes_consultation' => 'Viewing',
	'titre_page_themer' => 'Browse themes',
];

<?php
return [
	// M
	'meteo_1' => 'Sunny',
	'meteo_2' => 'Mostly Sunny',
	'meteo_3' => 'Partly Sunny',
	'meteo_4' => 'Intermittent Clouds',
	'meteo_5' => 'Hazy Sunshine',
	'meteo_6' => 'Mostly Cloudy',
	'meteo_7' => 'Cloudy',
	'meteo_8' => 'Dreary (Overcast)',
	'meteo_11' => 'Fog',
	'meteo_12' => 'Showers',
	'meteo_13' => 'Mostly Cloudy w/ Showers',
	'meteo_14' => 'Partly Sunny w/ Showers',
	'meteo_15' => 'T-Storms',
	'meteo_16' => 'Mostly Cloudy w/ T-Storms',
	'meteo_17' => 'Partly Sunny w/ T-Storms',
	'meteo_18' => 'Rain',
	'meteo_19' => 'Flurries',
	'meteo_20' => 'Mostly Cloudy w/ Flurries',
	'meteo_21' => 'Partly Sunny w/ Flurries',
	'meteo_22' => 'Snow',
	'meteo_23' => 'Mostly Cloudy w/ Snow',
	'meteo_24' => 'Ice',
	'meteo_25' => 'Sleet',
	'meteo_26' => 'Freezing Rain',
	'meteo_29' => 'Rain and Snow',
	'meteo_30' => 'Hot',
	'meteo_31' => 'Cold',
	'meteo_32' => 'Windy',
	'meteo_33' => 'Clear',
	'meteo_34' => 'Mostly Clear',
	'meteo_35' => 'Partly Cloudy',
	'meteo_36' => 'Intermittent Clouds',
	'meteo_37' => 'Hazy Moonlight',
	'meteo_38' => 'Mostly Cloudy',
	'meteo_39' => 'Partly Cloudy w/ Showers',
	'meteo_40' => 'Mostly Cloudy w/ Showers',
	'meteo_41' => 'Partly Cloudy w/ T-Storms',
	'meteo_42' => 'Mostly Cloudy w/ T-Storms',
	'meteo_43' => 'Mostly Cloudy w/ Flurries',
	'meteo_44' => 'Mostly Cloudy w/ Snow ',
];

<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/rainette_theme-paquet-xml-rainette_themes?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// R
	'rainette_theme_description' => 'This plugin is an add-on to Rainette plugin. It provides sets of icons and functions for exploring these sets, so you can select the one that suits you best.',
	'rainette_theme_nom' => 'Icon themes for Rainette',
	'rainette_theme_slogan' => 'Explore Rainette’s icon sets',
];

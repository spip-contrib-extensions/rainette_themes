<?php
return [
	// M
	'meteo_1000' => 'Clear/Sunny',
	'meteo_1003' => 'Partly Cloudy',
	'meteo_1006' => 'Cloudy',
	'meteo_1009' => 'Overcast',
	'meteo_1030' => 'Mist',
	'meteo_1063' => 'Patchy rain nearby',
	'meteo_1066' => 'Patchy snow nearby',
	'meteo_1069' => 'Patchy sleet nearby',
	'meteo_1072' => 'Patchy freezing drizzle nearby',
	'meteo_1087' => 'Thundery outbreaks in nearby',
	'meteo_1114' => 'Blowing snow',
	'meteo_1117' => 'Blizzard',
	'meteo_1135' => 'Fog',
	'meteo_1147' => 'Freezing fog',
	'meteo_1150' => 'Patchy light drizzle',
	'meteo_1153' => 'Light drizzle',
	'meteo_1168' => 'Freezing drizzle',
	'meteo_1171' => 'Heavy freezing drizzle',
	'meteo_1180' => 'Patchy light rain',
	'meteo_1183' => 'Light rain',
	'meteo_1186' => 'Moderate rain at times',
	'meteo_1189' => 'Moderate rain',
	'meteo_1192' => 'Heavy rain at times',
	'meteo_1195' => 'Heavy rain',
	'meteo_1198' => 'Light freezing rain',
	'meteo_1201' => 'Moderate or Heavy freezing rain',
	'meteo_1204' => 'Light sleet',
	'meteo_1207' => 'Moderate or heavy sleet',
	'meteo_1210' => 'Patchy light snow',
	'meteo_1213' => 'Light snow',
	'meteo_1216' => 'Patchy moderate snow',
	'meteo_1219' => 'Moderate snow',
	'meteo_1222' => 'Patchy heavy snow',
	'meteo_1225' => 'Heavy snow',
	'meteo_1237' => 'Ice pellets',
	'meteo_1240' => 'Light rain shower',
	'meteo_1243' => 'Moderate or heavy rain shower',
	'meteo_1246' => 'Torrential rain shower',
	'meteo_1249' => 'Light sleet showers',
	'meteo_1252' => 'Moderate or heavy sleet showers',
	'meteo_1255' => 'Light snow showers',
	'meteo_1258' => 'Moderate or heavy snow showers',
	'meteo_1261' => 'Light showers of ice pellets',
	'meteo_1264' => 'Moderate or heavy showers of ice pellets',
	'meteo_1273' => 'Patchy light rain in area with thunder',
	'meteo_1276' => 'Moderate or heavy rain in area with thunder',
	'meteo_1279' => 'Patchy light snow in area with thunder',
	'meteo_1282' => 'Moderate or heavy snow in area with thunder',
];

<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_RAINETTE_DEBUG_TRANSCODAGE')) {
	define('_RAINETTE_DEBUG_TRANSCODAGE', false);
}

/**
 * Construire le tableau des résumés météo d'un service donné indexé par les codes.
 *
 * @param string $service Identifiant du service
 *
 * @return array Tableau code => résumé en anglais
 */
function rainette_lister_codes(string $service) : array {
	$codes = [];

	// On acquiert la configuration du service
	include_spip('inc/rainette_normaliser');
	$configuration = configuration_service_lire($service, 'service');

	// Construction du tableau code => résumé en anglais
	$traduire = charger_fonction('traduire', 'inc');
	foreach (array_keys($configuration['icones']['transcodages']) as $_code) {
		$resume = $traduire("{$service}:meteo_{$_code}", 'en');
		$codes[$_code] = $resume ?: $_code;
	}

	// On rajoute le code na, le même pour tous
//	$codes['na'] = $traduire('rainette:meteo_na', 'en');

	return $codes;
}

/**
 * Construire un tableau indexé par les codes d'un service donné et fournissant les résumés et les icones
 * pour une période donnée (jour ou nuit).
 *
 * @param int   $periode          Période jour (0) ou nuit (1). Le défaut est 0 (jour).
 * @param array $parametres_icone Tableau de paramètres identifiant comment choisir l'icone :
 *                                - service : le nom du service dont est issu le code.
 *                                - transcodages : les tableaux de transcodages code / icone du service
 *                                - theme_origine : le type de thème parmi 'local', 'weather', 'wmo' (index de transcodage)
 *                                - theme_id : l'id du thème *
 *                                - periode_stricte : indique qu'on ne cherche que l'icone de la période (défaut false)
 *
 * @return array Tableau code => résumé + icone
 */
function rainette_lister_icones(int $periode, array $parametres_icone) : array {
	$icones = [];

	// Récupérer le tableau des codes et résumés du service dont on veut vérifier les icones ou le transcodage
	$codes = rainette_lister_codes($parametres_icone['service']);

	// Si on veut comparer le transcodage weather ou autre, alors on collecte les codes de ce service cible pour les
	// afficher dans la comparaison
	$codes_cible = [];
	if ($parametres_icone['theme_origine'] !== 'local') {
		$codes_cible = rainette_lister_codes($parametres_icone['theme_origine']);
	}

	// Construire le tableau complet des résumés et des icones
	// -- on tient compte de la période : si la source est vide c'est que l'icone n'existe pas pour cette période
	include_spip('inc/rainette_convertir');
	foreach ($codes as $_code => $_resume) {
		if ($source = code_meteo2icone($_code, $periode, $parametres_icone)) {
			$icones[$_code] = [
				'resume' => $_resume,
				'icone'  => [
					'code'       => $_code,
					'code_meteo' => $_code,
					'source'     => $source
				]
			];

			// Si la liste sert à de la comparaison avec du transcodage, on rajoute les données de ce transcodage
			if ($parametres_icone['theme_origine'] !== 'local') {
				$code_cible = $parametres_icone['transcodages'][$_code][$parametres_icone['theme_origine']][$periode];
				if ($code_cible) {
					$resume_cible = $codes_cible[$code_cible];
					$icones[$_code][$parametres_icone['theme_origine']] = [
						'resume' => $resume_cible,
						'code'   => $code_cible
					];
				}
			}
		}
	}

	return $icones;
}
